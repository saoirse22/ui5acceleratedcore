function citApp(sLocalResources, sViewType, sPlaceAt, sAppViewName, sErrorViewName, bIsMobile) {
	this.sLocalResources = sLocalResources;
	this.sViewType = sViewType;
	this.sPlaceAt = sPlaceAt;
	this.sAppViewName = sAppViewName;
	this.sErrorViewName = sErrorViewName;
	this.bIsMobile = bIsMobile;
	
	/**
	 * @param debugMode : String to distinguish whether the application is in debug mode or not.
	 * This is used for when switching between minifed and non-minifed files
	 */
	this.debugMode = "false";
	this.logLevel = "error";
	this.splitApp = null;
	this.eao = null;
	this.mm = null;
	citApp = this;

	/**
	 * Check to see if the "debug" parameter is in the URL & whether it is set to true
	 */
	 var checkForDebugMode = function () {
		var debugMode = $.sap.getUriParameters().get("debug");
		if(debugMode == null) {
			//register module path - minified
			this.debugMode = false;
		} else {
			if (debugMode==="true") {
				this.debugMode = true; //register module path for non-minified files
			} else {
				this.debugMode = false; //register module path - minified
			}
		}
	};

	var checkForLogLevel = function () {
		var logLevel = $.sap.getUriParameters().get("logLevel");
		if(logLevel != null && logLevel != "info" && logLevel != "warn" && logLevel != "debug") {
			this.logLevel = "error";
		} else {
			this.logLevel = logLevel;
		}
	};

	var loadMainView = function () {
		sap.ui.localResources(this.sLocalResources);
		
		try {
			//initialise the citApp view that actually initialize the content.
			var viewName = this.sLocalResources + "." + this.sAppViewName;
			var view = sap.ui.view({viewName:viewName, type:this.sViewType});
			if(this.sPlaceAt !=null && this.sPlaceAt !=""){
				view.placeAt(this.sPlaceAt);
			}
		} catch (e) {
			Logger.error(e.message);
			Logger.error(e.stack);
			var viewName = this.sLocalResources + "." + this.sErrorViewName;
			var view = sap.ui.view({viewName:viewName, type:this.sViewType});
			view.setModel(e,"errorModel");
			if(this.sPlaceAt !=null && this.sPlaceAt !=""){
				view.placeAt(this.sPlaceAt);
			}else{
				view.placeAt("body");
			}
		}
	};

	var loadBootstrap = function () {
		try {
			$.sap.registerModulePath("js", "js");
			$.sap.require("js.bootstrap");
			
			if (bootstrap != undefined) {
				//call the method to check whether the application is debug mode
				if (bootstrap.debugMode==="forcedTrue") { 
					this.debugMode = true; // DEV Mode when always non-minified versions of file is needed.
				}else if(bootstrap.debugMode==="true"){
					this.checkForDebugMode();// UAT when URL may influence having minified version or not.
				}else if(bootstrap.debugMode==="false"){
					this.debugMode = false;// Production where always minified version is used.
				}
				
				//iterate over the namespaces & register the module path
				if (bootstrap.namespaces != undefined) {
					for (var i = 0; i < bootstrap.namespaces.length; i++) {
						var item = bootstrap.namespaces[i];
						$.sap.registerModulePath(item.nameSpace, item.filesLocation);
						if (item.registerCaching) {
							sap.ui.core.AppCacheBuster.register(item.filesLocation);
						}
					}
				}
				//iterate over the css array & include style sheets
				if (bootstrap.cssFiles != undefined) {
					for (var i = 0; i < bootstrap.cssFiles.length; i++) {
						var item = bootstrap.cssFiles[i];
						$.sap.includeStyleSheet(item);
					}
				}
				
				//iterate over the js array & include js files
				if (bootstrap.jsFiles != undefined) {
					for (var i = 0; i < bootstrap.jsFiles.length; i++) {
						var item = bootstrap.jsFiles[i];
						if (this.debugMode) {
							$.sap.require(item);
						} else {
							$.sap.require(item+"-min");
						}
					}
				}
				// Check for Log level
				if (Logger !== undefined) {
					checkForLogLevel();
					//edit the functions based on the debug mode
					switch (this.logLevel){
					
					case "info": //overwrite nothing
						break;
					case "warn": //overwrite INFO
						Logger.info = function () {};
						break;
					case "debug": //overwrite WARN & INFO
						Logger.info = function () {};
						Logger.warn = function () {};
						break;
					case "error": //overwrite DEBUG, WARN & INFO
						Logger.info = function () {};
						Logger.warn = function () {};
						Logger.debug = function () {};
						break;
					}
				}
			}
		} catch (e) {
			
			console.error("Could not load bootstrap configuration. Because ; "+e.stack);
		}
	};

	/**
	 * Initialisation of citApplication
	 */
	var init = function () {
		try {
			loadBootstrap();
			loadMainView();
			$(document).ready(function(){
				citApp.initialiseMM();
			});
		} catch (e) {
			alert("Could not load application because " + e.message);
		}
	};

	init();
	return this;
};

citApp.prototype.initialiseEAO = function (sServiceUrl, bJson, sUsername, sPassword){
	var bInitialisedEAO = false;
	try{
		var eao = new EAO(sServiceUrl, bJson, sUsername, sPassword);
		this.eao = eao;
		bInitialisedEAO = true;
	}catch(e){
		Logger.error(e.stack);
		Logger.error(e.message);
	}
	
	return bInitialisedEAO;
},

citApp.prototype.initialiseMM = function(sPosition){
	var bInitialisedMM = false;
	try{
		var mm = new MessageManager(sPosition,this.bIsMobile);
		this.mm = mm;
		bInitialisedMM = true;
	}catch(e){
		Logger.error(e.stack);
		Logger.error(e.message);
	}
	
	return bInitialisedMM;
},

/**
*
 * @param sPage
* @param sViewType
* @returns oView - new view created having Id as qualified name of the View, with the passed in View Type
*
 * Developers need to make sure that the view is already not a part of the DOM.
* Also, if a particular view needs to be created more than once, then this function should not be used.
* Use framework method instead.
 */
citApp.prototype.loadNewView = function (sPage){
       var sId = this.replaceAllInString(sPage, ".", "_");
       var oView = sap.ui.view({id: sId, viewName:sPage, type:this.sViewType});
       return oView;
};
 
citApp.prototype.replaceAllInString = function(sVariable, existingCharacter, newCharacter){
       var sReplacedString =  sVariable;
       for(var i=0;i<sVariable.split(".").length-1;i++){
             sReplacedString = sReplacedString.replace(existingCharacter, newCharacter);
       }
       return sReplacedString;
};




/*
 * Methods below here are mobile only
 */
if(this.bIsMobile){
	citApp.prototype.toMasterPage = function (sPage, oData) {
		//check to see if sPage already exists in DOM
		var page = sap.ui.getCore().byId(sPage);
		
		if (page == undefined) {
			//load view
			var view = sap.ui.view({id: sPage, viewName:sPage, type:this.sViewType});
			//add view
			this.splitApp.addMasterPage(view);
		}
		
		//In the case where the page is already visible, but we want to re run the navigation
		if (this.splitApp.getCurrentMasterPage().getId() == sPage) {
			var evt = {};
			evt.data = oData;
			this.splitApp.getCurrentMasterPage().getController().onBeforeShow(evt);
		} else {
			this.splitApp.toMaster(sPage, "", oData, null);
		}
	};
	
	citApp.prototype.toDetailPage = function (sPage, oData) {
		//check to see if sPage already exists in DOM
		var page = sap.ui.getCore().byId(sPage);
		
		if (page == undefined) {
			//load view
			var view = sap.ui.view({id: sPage, viewName:sPage, type:this.sViewType});
			//add view
			this.splitApp.addDetailPage(view);
		}
		
		if (this.splitApp.getCurrentDetailPage().getId() == sPage) {
			var evt = {};
			evt.data = oData;
			this.splitApp.getCurrentDetailPage().getController().onBeforeShow(evt);
		} else {
			this.splitApp.toDetail(sPage, "", oData, null);
		}
	};
	
	citApp.prototype.backMasterPage = function (oBackData) {
		this.splitApp.backMaster(oBackData);
	};
	
	citApp.prototype.backDetailPage = function (oBackData) {
		this.splitApp.backDetail(oBackData);
	};
	
	citApp.prototype.setSplitApp = function (splitApp) {
		this.splitApp = splitApp;
	};
	
	citApp.prototype.getSplitApp = function () {
		return this.splitApp;
	};
}


/*
 * Method below here is desktop only
 */

citApp.prototype.navigate = function (sContainerId, sToViewId, oData, sTransition) {
	var oToView = sap.ui.getCore().byId(sToViewId);
	
	var oContainerView = sap.ui.getCore().byId(sContainerId);
	
	if (oToView == undefined) {
		//load the view
		var view = sap.ui.view({id: sToViewId, viewName:sToViewId, type:this.sViewType});
		
		//add content to the container 
		oContainerView.addContent(view);
		
		//pass oData
		if (view.getController().onBeforeShow != undefined) {
			view.getController().onBeforeShow(oData);
		}
		
		//do $ transition
		var visible = $('.currentlyVisible');
		visible.animate({"left":"-1000px"}, "slow").removeClass('currentlyVisible');
		
//	    } else {
//	        hidden.animate({"left":"0px"}, "slow").addClass('visible');
//	    }
	}
};



