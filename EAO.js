function EAO (domain, bJson, sUsername, sPassword) {
	new sap.m.BusyDialog("busyDialog");

	this.domain = domain;
	this.bJson = bJson;
	this.sUsername = sUsername;
	this.sPassword = sPassword;
	this.sEndPoint = "";
	this.oServiceModel = null;	
	this.sErrorMsg="";
	
	// check for local Domain 
	this.getServiceUrl(domain);
	this.oServiceModel = new sap.ui.model.odata.ODataModel(this.sEndPoint, bJson, sUsername, sPassword);
	
	/*if (this.oServiceModel.getServiceMetatdata() == null ){
		throw new Error("Tell them Why this is broken Here");
	}*/
}

EAO.prototype.openAlert = function() {
	sap.ui.commons.MessageBox.show("WARNING msg Failed to load data", "WARNING","Internal Error ");
};

EAO.prototype.requestSentEvent= function(uri) {
	Logger.info("Request Sent: " + uri);
	sap.ui.getCore().byId("busyDialog").open();
};

EAO.prototype.requestCompletedEvent = function (uri) {
	Logger.info("Request Completed: " + uri);
	sap.ui.getCore().byId("busyDialog").close();
};

// comment this section only show this if there is a 404  
EAO.prototype.requestFailedEvent = function (errMsg) {
	Logger.error("Request Failed" + JSON.stringify(errMsg));
	sap.ui.getCore().byId("busyDialog").close();
};

EAO.prototype.getServiceUrl = function (sBaseUrl) {

	try{
		if (window.location.hostname === "localhost") {
			//var sOrigin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ":" + window.location.port : "");
			this.sEndPoint = "proxy/" + sBaseUrl.replace("://", "/");
//			if (!jQuery.sap.startsWith(sBaseUrl, sOrigin)) {
//				
//			}
			return this.sEndPoint;
		} else {
			this.sEndPoint = sBaseUrl;
			return this.sEndPoint;
		}

	}catch(err){
		this.sErrorMsg = err;
	}
};

EAO.prototype.read = function(uri,callback) {
	this.requestSentEvent(uri);
	var fnResponse = {};
	var that = this;

	this.oServiceModel.read(uri, null, null, true,
		function (oData, response) {
			that.requestCompletedEvent(uri);
			fnResponse.model = oData;
			fnResponse.statusCode = 200;
			fnResponse.response = response;
			callback(fnResponse);
		},
		function (oError) {
			that.requestCompletedEvent(uri);
			that.sErrorMsg= oError;
			that.requestFailedEvent(that.sErrorMsg);
			fnResponse.statusCode = oError.response.statusCode;
			fnResponse.errorModel = that.messageParse(oError.response.body,null,null);
			callback(fnResponse);
		});
};

EAO.prototype.update = function(uri) {
	this.requestSentEvent();
	var fnResponse = {};
	var that = this;

	this.oServiceModel.create(uri, null, null, true,
		function (oData, response) {
			that.requestCompletedEvent(uri);
			fnResponse.response = response;
			fnResponse.model = oData;
			fnResponse.statusCode = 200;
			return fnResponse;
		},
		function (oError) {
			that.requestCompletedEvent(uri);
			that.sErrorMsg = oError;
			that.requestFailedEvent(that.sErrorMsg);
			fnResponse.statusCode = oError.response.statusCode;
			return fnResponse;
		});
};


EAO.prototype.create = function(uri,oModel) {
	this.requestSentEvent(uri);
	var fnResponse = {};
	var that = this;
	
	this.oServiceModel.create(uri, oModel, null, null, true,
		function (oData, response) {
			that.requestCompletedEvent(uri);
			fnResponse = response;
			fnResponse.model = oData;
			fnResponse.statusCode = 200;
		},
		function (oError) {
			that.requestCompletedEvent(uri);
			that.sErrorMsg = oError;
			that.requestFailedEvent(that.sErrorMsg);
			fnResponse.statusCode = oError.response.statusCode;
		});
};

/* Function to create an error model that is added to the fnResponseModel
*
*@input is of type string this is just the response message String contains various params
*@param1 / @param2 can be any additional information we need for the application for example
*dates or user information if an error is specific
*
*/
EAO.prototype.messageParse = function(input,param1,param2) {
	
	var messageModel = {};
	
	try {
		
		var parsedString = JSON.parse(input);
		
		if (parsedString.error.message !== undefined) {
			messageModel.message = parsedString.error.message.value;
		}
		
		if (parsedString.error.innererror.errordetails[0] !== undefined) {
			var errorDetail = parsedString.error.innererror.errordetails[0];
		
			messageModel.message = errorDetail.message;
			messageModel.messageLevel = errorDetail.level;
			messageModel.messageCode = errorDetail.code;
			messageModel.param1 = param1;
			messageModel.param2 = param2;	
		} else {
			//generic error message... blah blah contact manager
			messageModel.message = "errorDetail.message";
			messageModel.messageLevel = "errorDetail.level";
			messageModel.messageCode = "errorDetail.code";
			messageModel.param1 = null;
			messageModel.param2 = null;
		}
	
	} catch (e) {
		messageModel.message = "Error when making call to retrieve data.";
	}
	
	return messageModel;
};